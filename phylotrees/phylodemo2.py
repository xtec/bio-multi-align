from Bio import Phylo
# The dnd was copied and pasted from:
#    https://open.oregonstate.education/appliedbioinformatics/chapter/chapter-4/
# Pending to calculate the aln and generate dnd.
clustree = Phylo.read("18S_rna.dnd","newick")
Phylo.draw_ascii(clustree)