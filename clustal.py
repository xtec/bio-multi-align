from Bio.Align import PairwiseAligner, substitution_matrices
from ete3 import Tree
import numpy as np
from neighbour_joining import njTree
from util import sequence

# Homo sapiens, Camelus dromedarius, Pan troglodytes, Canis lupus familiaris
seqs = sequence(
    "protein", "AAA59595.1", "KAB1270346.1", "XP_024208663.1", "XP_038535952.1"
)

# Distance matrix
aligner = PairwiseAligner(
    mode="local",
    substitution_matrix=substitution_matrices.load("BLOSUM62"),
    open_gap_score=-10,
    extend_gap_score=-0.5,
)

n = len(seqs)
D = np.zeros((n, n))

max = 0
for i in range(0, n):
    for j in range(i + 1, n):
        score = aligner.score(seqs[i], seqs[j])
        D[i, j] = score
        if score > max:
            max = score

for i in range(0, n):
    for j in range(i + 1, n):
        score = D[i, j]
        distance = 1 - score / max
        D[i, j] = distance
        D[j, i] = distance
with np.printoptions(precision=5):
    print(D)


print("Matriu de distancies")
tree = njTree(D, np.asarray([seq.id for seq in seqs]))
print(tree)

print("Arbre filogenètic")
tree = Tree(tree)
print(tree)
