from Bio import Entrez, SeqIO, SeqRecord
import os

Entrez.email = "david@xtec.dev"

def print_matrix(name):
    ""
    
# nuccore, protein

def sequence(db: str, *ids):
    
  result = []

  for id in ids:

    dir = "data/"
    if not os.path.isdir(dir):
        os.makedirs(dir)

    file = "{}/{}.fasta".format(dir, id)
    if not os.path.exists(file):
        print(f"Downloading file {id}...")
        with Entrez.efetch(db=db, id=id, rettype="fasta") as response:
            with open(file, "w") as out:
                out.write(response.read())

    result.append(SeqIO.read(file, "fasta"))
  
  return result


