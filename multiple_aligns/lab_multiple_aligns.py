import urllib
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

# We can download files manually.
for f in ["PF05371_seed.sth", "dummy_aln.phy"]:
    urllib.request.urlretrieve(f"https://milliams.com/courses/biopython/{f}", f)

# Read single alignments
aln_seed = AlignIO.read("PF05371_seed.sth", "stockholm")
print(aln_seed)

for record in aln_seed:
    print(f"{record.seq[:50]} - {record.id}")

# Parse multiple alignments
aln_dummy = AlignIO.parse("dummy_aln.phy", "phylip")
for alignment in aln_dummy:
    print(alignment)
    print("---")

alignments = list(AlignIO.parse("dummy_aln.phy", "phylip"))
second_aln = alignments[1]
print(second_aln)

# Write alignments
align1 = MultipleSeqAlignment([
    SeqRecord(Seq("ACTGCTAGCTAG"), id="toto"),
    SeqRecord(Seq("ACT-CTAGCTAG"), id="titi"),
    SeqRecord(Seq("ACTGCTAGDTAG"), id="tata"),
])

print(align1)

my_alignments = [align1, aln_seed]
AlignIO.write(my_alignments, "mixed.phy", "phylip")